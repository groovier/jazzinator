import { BaseElement } from './base_element';
import { Chord } from './chord';
import { Voice } from './voice';
import { StackOfThirds } from './stack_of_thirds';

let rootNote = new BaseElement(BaseElement.nameToVal('c'));

let callback = function(voice: Voice) {
    console.log(voice.pitch);
};

console.log('Chord type: 7,9');
let chord = new Chord(rootNote);
chord.elements.setValue(7, rootNote.step(10, false));
chord.elements.setValue(9, rootNote.step(13, false));
let style: StackOfThirds = new StackOfThirds();
console.log(style.getVariants());
console.log('');
console.log('Variant: Rooted');
style.harmonize(chord);
chord.voicing.forEach(callback);
console.log('');
console.log('Variant: First inversion');
style.setVariantNumber(1);
style.harmonize(chord);
chord.voicing.forEach(callback);
console.log('');
console.log('Variant: Second inversion');
style.setVariantNumber(2);
style.harmonize(chord);
chord.voicing.forEach(callback);
console.log('');
console.log('Chord type: 7,9sus4');
chord = new Chord(rootNote);
// Turn it into a sus4.
chord.elements.remove(3);
chord.elements.setValue(4, rootNote.step(5));
chord.elements.setValue(7, rootNote.step(10, false));
chord.elements.setValue(9, rootNote.step(13, false));
style = new StackOfThirds();
console.log(style.getVariants());
console.log('');
console.log('Variant: Rooted');
style.harmonize(chord);
chord.voicing.forEach(callback);
console.log('');
console.log('Variant: First inversion');
style.setVariantNumber(1);
style.harmonize(chord);
chord.voicing.forEach(callback);
console.log('');
console.log('Variant: Second inversion');
style.setVariantNumber(2);
style.harmonize(chord);
chord.voicing.forEach(callback);