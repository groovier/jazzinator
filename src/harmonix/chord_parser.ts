import { BaseElement } from './base_element';
import { Chord } from './chord';

class Iterator {
    length: number;
    done = false;
    private cursor = 0;

    constructor(length: number) {
        this.length = length;
    }

    next() {
        this.setCursor(this.cursor + 1);
        return this.cursor;
    }

    forward(step: number) {
        if (this.left(step)) {
            this.setCursor(this.cursor + step);
            return true;
        } else {
            return false;
        }
    }

    left(size: number) {
        return this.length - this.cursor >= size ? true : false;
    }

    getCursor() {
        return this.cursor;
    }

    private setCursor(cursor: number) {
        this.cursor = cursor;
        // If we stand on the last character, then we are NOT done. So if there are
        // 3 characters, and the length thus i 3, and we stand on the last one
        // which is no. 2, we are NOT done. But next time we move forward, we will
        // be done.
        if (this.cursor >= this.length) {
            this.done = true;
        }
    }
}

class ChordParser {

    data: string;
    rootSymbol: string;  // e. g. 'Gm'
    root: BaseElement;
    chord: Chord;
    iterator: Iterator;

    constructor(data: string) {
        this.data = data;
        this.iterator = new Iterator(data.length);
    }

    isChordLetter(str: string) {
        return str && str.length === 1 && str.toUpperCase() === str && str.match(/[A-H]/i);

    }

    /** Looks forward from the current cursor position to see if the next part of¨
     * data is identical to 'text'.
     * If yes: return 'true'.
     * If no: return 'false'.
     */
    findForward(text: string) {
        let length = text.length;
        let candidate: string;
        if (this.iterator.left(length)) {
            candidate = this.data.substr(this.iterator.getCursor(), length);
            if (candidate === text) {
                return true;
            }
        }
        return false;
    }

    /** Parses the chord root note and creates a major triad chord from the root note.
     * This chord servers as the base for later modifications if needed.
     */
    parseBasicChord() {
        this.rootSymbol = this.data[0];
        let sharpNature: boolean | undefined = undefined;
        // See if this is a chord at all, and if not, there is not more to do.
        if (!this.isChordLetter(this.rootSymbol)) {
            return false;
        }
        // The first character was OK. Move forward by 1!
        this.iterator.next();
        // Check for b.
        if (this.findForward('b')) {
            this.rootSymbol += 'b';
            sharpNature = false;
            this.iterator.next();
        }
        // Check for #.
        if (this.findForward('#')) {
            this.rootSymbol += '#';
            sharpNature = true;
            this.iterator.next();
        }
        // Create a normal chord.
        console.log('Root symbol: ' + this.rootSymbol);
        this.root = new BaseElement(BaseElement.nameToVal(this.rootSymbol), sharpNature);
        this.chord = new Chord(this.root);
        return true;
    }

    /** Checks if the next sequence is maj7, mmaj7, 7, m7, or m, and modifies the
     * chord accordingly.
     */
    parseChordType() {
        let searchString: string;

        searchString = 'maj7';
        if (this.findForward(searchString)) {
            this.chord.elements.setValue(7, this.root.step(11));
            this.iterator.forward(searchString.length);
            return true;
        }

        searchString = 'mmaj7';
        if (this.findForward(searchString)) {
            this.chord.minorize();
            this.chord.elements.setValue(7, this.root.step(11));
            this.iterator.forward(searchString.length);
            return true;
        }

        searchString = '6';
        if (this.findForward(searchString)) {
            this.chord.elements.setValue(6, this.root.step(9));
            this.iterator.forward(searchString.length);
            return true;

        }

        searchString = '7';
        if (this.findForward(searchString)) {
            this.chord.elements.setValue(7, this.root.step(10, false));
            this.iterator.forward(searchString.length);
            return true;
        }

        searchString = 'm6';
        if (this.findForward(searchString)) {
            this.chord.minorize();
            this.chord.elements.setValue(6, this.root.step(9));
            this.iterator.forward(searchString.length);
            return true;
        }

        searchString = 'm7';
        if (this.findForward(searchString)) {
            this.chord.minorize();
            this.chord.elements.setValue(7, this.root.step(10, false));
            this.iterator.forward(searchString.length);
            return true;
        }

        searchString = 'm';
        if (this.findForward(searchString)) {
            this.chord.minorize();
            this.iterator.forward(searchString.length);
            return true;
        }

        return false;
    }
    
    /** Checks for modifiers such as 'b5' or '#9' and modifies the chord if needed.
     */
    parseChordModifiers() {
        let searchString: string;
        let chordModified: boolean = false;
        while (!this.iterator.done) {
            searchString = ',';
            if (this.findForward(searchString)) {
                this.iterator.forward(1);
                chordModified = false;
            }   
            searchString = 'b5';
            if (this.findForward(searchString)) {
                console.log('Modifier found: ' + searchString);
                this.chord.elements.setValue(5, this.root.step(6, false));
                this.iterator.forward(searchString.length);
                chordModified = true;
            }
            searchString = 'dim';
            if (this.findForward(searchString)) {
                console.log('Modifier found: ' + searchString);
                this.chord.elements.setValue(3, this.root.step(3, false));
                this.chord.elements.setValue(5, this.root.step(6, false));
                this.chord.elements.setValue(7, this.root.step(9, false));
                this.iterator.forward(searchString.length);
                chordModified = true;
            }
            searchString = '+';
            if (this.findForward(searchString)) {
                console.log('Modifier found: ' + searchString);
                this.chord.elements.setValue(5, this.root.step(8, true));
                this.iterator.forward(searchString.length);
                chordModified = true;
            }
            searchString = '9';
            if (this.findForward(searchString)) {
                console.log('Modifier found: ' + searchString);
                this.chord.elements.setValue(9, this.root.step(14));
                this.iterator.forward(searchString.length);
                chordModified = true;
            }
            searchString = 'b9';
            if (this.findForward(searchString)) {
                console.log('Modifier found: ' + searchString);
                this.chord.elements.setValue(9, this.root.step(13, false));
                this.iterator.forward(searchString.length);
                chordModified = true;
            }
            searchString = '#9';
            if (this.findForward(searchString)) {
                console.log('Modifier found: ' + searchString);
                this.chord.elements.setValue(9, this.root.step(15, true));
                this.iterator.forward(searchString.length);
                chordModified = true;
            }
            searchString = '11';
            if (this.findForward(searchString)) {
                console.log('Modifier found: ' + searchString);
                this.chord.elements.setValue(11, this.root.step(17));
                this.iterator.forward(searchString.length);
                chordModified = true;
            }
            searchString = '#11';
            if (this.findForward(searchString)) {
                console.log('Modifier found: ' + searchString);
                this.chord.elements.setValue(11, this.root.step(18, true));
                this.iterator.forward(searchString.length);
                chordModified = true;
            }
            searchString = '13';
            if (this.findForward(searchString)) {
                console.log('Modifier found: ' + searchString);
                this.chord.elements.setValue(13, this.root.step(21));
                this.iterator.forward(searchString.length);
                chordModified = true;
            }
            searchString = 'sus4';
            if (this.findForward(searchString)) {
                console.log('Modifier found: ' + searchString);
                this.chord.elements.remove(3);
                this.chord.elements.setValue(4, this.root.step(5));
                this.iterator.forward(searchString.length);
                chordModified = true;
            }         
            // Done for now...
            this.iterator.next();
        }
        return chordModified;
    }

    addImplicitChordsteps() {
        let chordModified: boolean = false;
        if (this.chord.elements.containsKey(13) && !this.chord.elements.containsKey(11)) {
            this.chord.elements.setValue(11, this.root.step(17));
            chordModified = true;
        }
        if (this.chord.elements.containsKey(11) && !this.chord.elements.containsKey(9)) {
            this.chord.elements.setValue(9, this.root.step(14));
            chordModified = true;
        }
        if (this.chord.elements.containsKey(9) && !this.chord.elements.containsKey(7)) {
            this.chord.elements.setValue(7, this.root.step(10, false));
            chordModified = true;
        }
        return chordModified;
    }
}

export default ChordParser;