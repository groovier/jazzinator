import { Chord } from './chord';
import { Harmonizer, Variant } from './harmonizer';
import { Voice } from './voice';

export class BasicJazz extends Harmonizer {

    getTitle() {
        return '4-Voice Level One';
    }

    getDescription() {
        // 78 chars.
        return 'A simple four-note voicing. Adds a 7 or a maj7 if not present in chord symbol.';
    }

    getVariants() {
        return [
            new Variant('1 3 7', [3, 7]),
            new Variant('1 7 3', [7, 3]),
        ];
    }

    getDesiredVoiceCount(): number {
        return 4;
    }

    harmonize(chord: Chord) {
        this.deleteVoices(chord);
        this.addVoiceFromElementNumber(chord, 1);
        /* Try to add both of the elements in param list (either 3 first then 7
        *  or the other way around). If one of them fail, it must be because
        *  there is no 7th in the chord. In that case, add the 7th explicitly.
        */
        if (!this.addVoiceFromElementNumber(chord, this.getParams()[0]) ||
            !this.addVoiceFromElementNumber(chord, this.getParams()[1])) {
            /* No 7th in the chord. Add either a high or a low 7th. If major,
            * use high 7th, else use the low 7th (seems to work best for both
            * minor end sus4 chords).
            */
            let voice = new Voice();
            if (chord.isMajor()) {
                voice.element = chord.elements.getValue(1).step(11);
            } else {
                voice.element = chord.elements.getValue(1).step(10);
            }
            this.addVoice(chord, voice);
        }
        this.addMissingVoices(chord);
        while (this.checkSmallSecondInTop(chord)) {
            this.dropTopVoice(chord);
        }
        return true;
    }
}