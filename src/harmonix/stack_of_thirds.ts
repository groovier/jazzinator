import { Chord } from './chord';
import { Harmonizer, Variant } from './harmonizer';

export class StackOfThirds extends Harmonizer {

    getTitle() {
        return 'Stack Of Thirds';
    }

    getDescription() {
        return 'This is the way most people learn to play chords on piano. ' +
        'Only included here for reference. You should NOT play piano like this.';
    }

    getVariants() {
        // Tight only has one parameter which is the chord step of the first note.
        return [
            new Variant('Rooted', [1]),
            new Variant('First inversion', [3]),
            new Variant('Second inversion', [5])
        ];
    }

    harmonize(chord: Chord) {
        this.deleteVoices(chord);
        /* Get the root element.
        / Below, [0] refers to the first parameter in the parameter list. So we
        / send either 1, 3, or 5 as argument to addStackOfThirds. */
        let startElementNumber: number = this.getParams()[0];
        this.addStackOfThirds(chord, startElementNumber);
        // If we did not start with chord step 0, find the remaining notes.
        if (this.getParams()[0] > 0) {
            this.addMissingVoices(chord);
        }
        return true;
    }
}