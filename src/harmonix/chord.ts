import * as Collections from 'typescript-collections';
import { BaseElement } from './base_element';
import { Voice } from './voice';
import { Harmonizer } from './harmonizer';

export enum Bender {
    flat,
    perfect,
    sharp
}

export enum ThirdType {
    minor,
    major,
    sus4
}

export enum FifthType {
    b5,
    perfect,
    plus
}

export enum SeventhType {
    sixth,
    dominant,
    noSeventh,
    maj7
}

export class Chord {

    /**
     * An optional name of the chord, f. ex. 'C7'.
     */
    name: string = '';

    primarySymbol: number = 0;
    bender: Bender = Bender.perfect;
    thirdType: ThirdType = ThirdType.major;
    fifthType: FifthType = FifthType.perfect;
    seventhType: SeventhType = SeventhType.maj7;

    /**
     * Contains the BaseElements that make up the chord, organized in a
     * dictionary-based structure where the keys represent the 'factor' as defined here:
     * https://en.wikipedia.org/wiki/Factor_(chord).
     * 1 represents the root factor, 3 is the third, etc. One of the advantages with
     * this system is that your are not limited to the normally used factors, i. e.
     * you can use other numbers than the uneven numbers from 1 to 13. Other
     * advantage is that it's easy to get the wanted BaseElement from the key and to sort
     * the elements.
     */
    elements: Collections.Dictionary<number, BaseElement>;
    voicer: Harmonizer;
    voicing: Collections.LinkedList<Voice>;

    static getChordLetters(euro: boolean) {
        if (euro) {
            return ['C', 'D', 'E', 'F', 'G', 'A', 'H'];
        }
        return ['C', 'D', 'E', 'F', 'G', 'A', 'B'];
    }

    /**
     * The constructor always creates a major triad as this is the most basic chord type.
     * @param root BaseElement
     */
    constructor(root: BaseElement) {
        this.elements = new Collections.Dictionary<number, BaseElement>();
        this.elements.setValue(1, root);
        this.elements.setValue(3, root.step(4));
        this.elements.setValue(5, root.step(7));
        this.voicing = new Collections.LinkedList<Voice>();
    }

    isMinor() {
        if (this.thirdType === ThirdType.minor) {
            return true;
        }
        return false;
    }

    isMajor() {
        if (this.thirdType === ThirdType.major) {
            return true;
        }
        return false;
    }

    isSus4() {
        if (this.thirdType === ThirdType.sus4) {
            return true;
        }
        return false;
    }

    isB5() {
        if (this.fifthType === FifthType.b5) {
            return true;
        }
        return false;
    }

    isPerfectFifth() {
        if (this.fifthType === FifthType.perfect) {
            return true;
        }
        return false;
    }

    isSixth() {
        if (this.seventhType === SeventhType.sixth) {
            return true;
        }
        return false;
    }
    
    isDominant() {
        if (this.seventhType === SeventhType.dominant) {
            return true;
        }
        return false;
    }

    isMaj7() {
        if (this.seventhType === SeventhType.maj7) {
            return true;
        }
        return false;
    }

    isPlus() {
        if (this.fifthType === FifthType.plus) {
            return true;
        }
        return false;
    }

    getChordBase(euro?: boolean) {
        if (euro === undefined) {
            euro = false;
        }
        let chordBase: string = Chord.getChordLetters(euro)[this.primarySymbol];
        if (this.bender === Bender.sharp) {
            chordBase += '#';
        }
        if (this.bender === Bender.flat) {
            chordBase += 'b';
        }
        return chordBase;
    }
    
    getChordName(euro?: boolean) {
        if (euro === undefined) {
            euro = false;
        }
        let chordName: string = this.getChordBase(euro);

        if (this.isMinor() && this.isSixth() && this.isB5()) {
            return chordName + 'dim';
        }

        if (this.isMinor()) {
            chordName += 'm';
        }
        if (this.isSixth()) {
            chordName += '6';
        }
        if (this.isDominant()) {
            chordName += '7';
        }
        if (this.isMaj7()) {
            chordName += 'maj7';
        }
        if (this.isSus4()) {
            chordName += 'sus4';
        }
        if (this.isB5()) {
            chordName += 'b5';
        }
        if (this.isPlus()) {
            chordName += '+';
        }
        if (this.isMinor() && this.isDominant() && this.isB5()) {
            chordName = chordName + ' (' + this.getChordBase(euro) + 'ø)';
        }
        return chordName;
    }

    /**
     * Changes the chord to minor.
     */
    minorize() {
        this.elements.setValue(3, this.elements.getValue(1).step(3, false));
    }

    getElements() {
        let elementList: BaseElement[] = [];
        let size = this.elements.size();
        let numberAdded = 0;
        let index = 1;
        while (numberAdded < size) {
            if (this.elements.containsKey(index)) {
                elementList.push(this.elements.getValue(index));
                numberAdded++;
            }
            index++;
        }
        return elementList;
    }

    getElementNames() {
        let elementList: string[] = [];
        let size = this.elements.size();
        let numberAdded = 0;
        let index = 1;
        while (numberAdded < size) {
            if (this.elements.containsKey(index)) {
                elementList.push(this.elements.getValue(index).getName());
                numberAdded++;
            }
            index++;
        }
        return elementList;
    }

    toString() {
        return this.getElementNames().join(' ');
    }

    getVoices() {
        let voices: Voice[] = [];
        let callback = function(voice: Voice) {
            voices.push(voice);
        };
        this.voicing.forEach(callback);
        return voices;
    }

    getRoleNumber(pitchClass: number) {
        for (let roleNumber of [1, 3, 5, 7]) {
            let someElement: BaseElement = this.elements.getValue(roleNumber);
            if (someElement && someElement.pitchClass === pitchClass) {
                return roleNumber;
            }
        }
        return null;
    }

    containsPitchClass(pitchClass: number) {
        for (let element of this.elements.values()) {
            if (element.pitchClass === pitchClass) {
                return true;
            }
        }
        return false;
    }

    getVoicingSize() {
        return this.voicing.indexOf(this.voicing.last()) + 1;
    }

    build() {
        let pitchClass: number = BaseElement.nameToVal(this.getChordBase().toLowerCase());

        let sharpNature: boolean = undefined;
        if (this.bender === Bender.sharp) {
            sharpNature = true;
        }
        if (this.bender === Bender.flat) {
            sharpNature = false;
        }
        this.elements.clear();
        let root: BaseElement = new BaseElement(pitchClass, sharpNature);
        this.elements.setValue(1, root);
        if (this.isMinor()) {
            this.elements.setValue(3, root.step(3, root.sharpNature));
        } else {
            this.elements.setValue(3, root.step(4, root.sharpNature));
        }
        if (this.isSus4()) {
            this.elements.remove(3);
            this.elements.setValue(3, root.step(5, root.sharpNature));
        }
        if (this.isB5()) {
            this.elements.setValue(5, root.step(6, root.sharpNature));
        }
        if (this.isPlus()) {
            this.elements.setValue(5, root.step(8, root.sharpNature));
        }
        if (!this.isB5() && !this.isPlus()) {
            this.elements.setValue(5, root.step(7, root.sharpNature));
        }
        if (this.isSixth()) {
            this.elements.setValue(7, root.step(9, root.sharpNature));
        }
        if (this.isDominant()) {
            this.elements.setValue(7, root.step(10, root.sharpNature));
        }
        if (this.isMaj7()) {
            this.elements.setValue(7, root.step(11, root.sharpNature));
        }
    }
}