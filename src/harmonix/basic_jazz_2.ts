import { BaseElement } from './base_element';
import { Chord } from './chord';
import { BasicJazz } from './basic_jazz';
import { Voice } from './voice';
import { Alternative } from './harmonizer';

export class BasicJazz2 extends BasicJazz {

    getTitle() {
        return '4-voice Level Two';
    }

    getDescription() {
        return 'Like 4-Voice Level One, but instead of using the top voice for the fifth, use an extension.';
    }

    getDesiredVoiceCount(): number {
        return 4;
    }

    getAlternatives(chord: Chord): Alternative[] {
        if (chord.isMajor() && chord.isMaj7()) {  // maj7
                return [
                new Alternative(14, '9'),
                new Alternative(9, '13'),
                new Alternative(6, '(#11)'),
            ];
        }
        if (chord.isMinor() && chord.isDominant() && chord.isB5()) {  // m7b5
            return [
                new Alternative(5, '11'),
                new Alternative(2, '(9)'),
            ];
        }
        if (chord.isMinor() && chord.isDominant()) {  // m7
            return [
                new Alternative(2, '9'),
                new Alternative(11, '5'),
            ];
        }
        if (chord.isMinor() && chord.isSixth() && chord.isB5()) {  // dim
            return [
                new Alternative(2, '9'),
                new Alternative(5, '11'),
            ];
        }
        if (chord.isMinor() && chord.isMaj7() && chord.isPlus()) { // maj7+
            return [
                new Alternative(5, '11'),
            ];
        }
        if (chord.isMinor() && chord.isMaj7()) { // mmaj7
            return [
                new Alternative(2, '9'),
            ];
        }
        if (chord.isMajor() && chord.isDominant()) { // 7
            return [
                new Alternative(1, 'b9'),
                new Alternative(2, '9'),
                new Alternative(3, '#9'),
                new Alternative(8, 'b13'),
                new Alternative(9, '13'),
            ];
        }
        return [
            new Alternative(14, '9'),
            new Alternative(9, '13'),
            new Alternative(6, '(#11)'),
        ];
    }

    harmonize(chord: Chord) {
        this.deleteVoices(chord);
        this.addVoiceFromElementNumber(chord, 1);
        /* Try to add both of the elements in param list (either 3 first then 7
        *  or the other way around). If one of them fail, it must be because
        *  there is no 7th in the chord. In that case, add the 7th explicitly.
        */
        if (!this.addVoiceFromElementNumber(chord, this.getParams()[0]) ||
            !this.addVoiceFromElementNumber(chord, this.getParams()[1])) {
            /* No 7th in the chord. Add either a high or a low 7th. If major,
            * use high 7th, else use the low 7th (seems to work best for both
            * minor end sus4 chords).
            */
            let voice = new Voice();
            if (chord.isMajor()) {
                voice.element = chord.elements.getValue(1).step(11);
            } else {
                voice.element = chord.elements.getValue(1).step(10);
            }
            this.addVoice(chord, voice);
        }

        // Add extension 1
        /* If an alternative with the number indicated by alternatives1 does not exist,
        *  reset alternative1 to 0.
        */
        if (!this.getAlternatives(chord)[this.getAlternative1()]) {
            this.resetAlternative1();
        }
        let alternative = this.getAlternatives(chord)[this.getAlternative1()];
        let ext1Element: BaseElement = chord.elements.getValue(1).step(alternative.stepNumber);
        let ext1Voice: Voice = new Voice;
        ext1Voice.element = ext1Element;
        this.addVoice(chord, ext1Voice);

        while (this.checkSmallSecondInTop(chord)) {
            this.dropTopVoice(chord);
        }
        return true;
    }
}