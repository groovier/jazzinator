export class BaseElement {

    /**
     * https://en.wikipedia.org/wiki/Pitch_class
     */
    pitchClass: number;

    sharpNature: boolean = false;

    /**
     * Static method that is helpful when constructing a BaseElement.
     */ 
    static nameToVal(name: string) {
        name = name.toLowerCase();
        switch (name) {
            case 'c':
                return 0;
            case 'c#':
                return 1;
            case 'db':
                return 1;
            case 'd':
                return 2;
            case 'd#':
                return 3;
            case 'eb':
                return 3;
            case 'e':
                return 4;
            case 'f':
                return 5;
            case 'f#':
                return 6;
            case 'gb':
                return 6;
            case 'g':
                return 7;
            case 'g#':
                return 8;
            case 'ab':
                return 8;
            case 'a':
                return 9;
            case 'a#':
                return 10;
            case 'bb':
                return 10;
            case 'hb':
                return 10;
            case 'b':
                return 11;
            case 'h':
                return 11;
            default:
                throw new RangeError;
        }
    }

    constructor(pitchClass: number, sharpNature?: boolean) {
        this.pitchClass = pitchClass;
        if (sharpNature === undefined) {
            if ([0, 1, 3, 5, 6, 8, 10].indexOf(pitchClass) >= 0) {
                this.sharpNature = false;
            } else {
                this.sharpNature = true;
            }
        } else {
            this.sharpNature = sharpNature;
        }
    }

    getName(euro?: boolean) {
        if (euro === undefined) {
            euro = false;
        }
        switch (this.pitchClass) {
            case 0:
                return 'c';
            case 1:
                return this.sharpNature ? 'c#' : 'db';
            case 2:
                return 'd';
            case 3:
                return this.sharpNature ? 'd#' : 'eb';
            case 4:
                return 'e';
            case 5:
                return 'f';
            case 6:
                return this.sharpNature ? 'f#' : 'gb';
            case 7:
                return 'g';
            case 8:
                return this.sharpNature ? 'g#' : 'ab';
            case 9:
                return 'a';
            case 10:
                if (this.sharpNature) {
                    return 'a#';
                }
                return euro ? 'hb' : 'bb';
            case 11:
                return euro ? 'h' : 'b';
            default:
            throw new RangeError;
        }
    }

    step(n: number, sharpNature?: boolean) {
        let newElement = new BaseElement((this.pitchClass + n) % 12);
        if (sharpNature === undefined) {
            newElement.sharpNature = this.sharpNature;
        } else {
            newElement.sharpNature = sharpNature;
        }
        return newElement;
    }

    toString() {
        return this.pitchClass;
    }
}