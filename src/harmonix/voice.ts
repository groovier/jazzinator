import { BaseElement } from './base_element';

/**
 * Returns the octave of a given pitch.
 * @param pitch 
 */
/*function o(pitch: number) {
    return Math.floor(pitch / 12) - 1;
}*/

/**
 * Returns a pitch of <pitchclass> within <octave>.
 * @param pitchClass 
 * @param octave 
 */
/*function p(pitchClass: number, octave: number) {
    let arithmethicOctave = octave + 1;
    let baseC = arithmethicOctave * 12;
    return baseC + pitchClass;
}*/

export class Voice {

    element: BaseElement;

    pitch: number;
}