import { BaseElement } from './base_element';
import { Chord } from './chord';
let rootNote = new BaseElement(BaseElement.nameToVal('c'));
let chord = new Chord(rootNote);
chord.elements.setValue(7, rootNote.step(10, false));
chord.elements.setValue(9, rootNote.step(13, false));
console.log(chord);