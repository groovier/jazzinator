import * as fs from 'fs';
import ChordParser from './chord_parser';

function parse_textfile(fileName: string) {
    let data: string = String(fs.readFileSync(fileName));
    data = data.replace(/\n|\r/g, ' ');  // Replace newlines with spaces
    let words: Array<string> = data.split(' ');
    for (let word of words) {
        console.log('Now parsing: ' + word);
        let parser: ChordParser = new ChordParser(word);
        if (parser.parseBasicChord()) {
            console.log('Created basic chord: ' + parser.chord.toString());
        } else {
            console.log('Not a chord.');
            console.log('');
            continue;
        }
        if (!parser.iterator.done) {
            if (parser.parseChordType()) {
                console.log('Changed chord type: ' + parser.chord.toString());
            }
        }
        if (!parser.iterator.done) {
            if (parser.parseChordModifiers()) {
                console.log('Modified chord to ' + parser.chord.toString());
            }
        }
        if (parser.addImplicitChordsteps()) {
            console.log('Modified chord to ' + parser.chord.toString());
        }
        console.log('');
    }
}

export default parse_textfile;

parse_textfile('file.txt');