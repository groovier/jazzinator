import { Harmonizer } from './harmonizer';
import { StackOfThirds } from './stack_of_thirds';
import { BasicJazz } from './basic_jazz';
import { BasicJazz2 } from './basic_jazz_2';
import { FiveVoice } from './five_voice';

export class StyleBook {
    styles: Harmonizer[];

    constructor() {
        this.styles = [];
        this.styles.push(new StackOfThirds());
        this.styles.push(new BasicJazz());
        this.styles.push(new BasicJazz2());
        this.styles.push(new FiveVoice());
    }

    index() {
        let output = Array();
        for (let style of this.styles) {
            output.push(style.getTitle());
        }
        return output;
    }
}