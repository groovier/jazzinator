import { Chord } from './chord';
import { Voice } from './voice';

/**
 * Returns the pitch class (= note name) of a MIDI pitch value.
 *  @param pitch
 */
function pc(pitch: number) {
    let numberOfWholeOctaves = Math.floor(pitch / 12);
    return pitch - (numberOfWholeOctaves * 12);
}

function isOnTopOf(pitch: number, voice: Voice) {
    if (voice === undefined) {
        return true;
    }
    return pitch >= voice.pitch;
}

function voicesAreEqualByPitchClass(voice1: Voice, voice2: Voice) {
    return voice1.element.pitchClass === voice2.element.pitchClass;
}

export class Variant {
    variantName: string;

    paramList: number[];

    constructor(variantName: string, paramList: number[]) {
        this.variantName = variantName;
        this.paramList = paramList;
    }
}

export class Alternative {
    stepNumber: number;
    description: string;

    constructor(stepNumber: number, description: string) {
        this.stepNumber = stepNumber;
        this.description = description;
    }
}

export abstract class Harmonizer {
    variantNumber: number = 0;
    private alternative1: number = 0;
    private alternative2: number = 0;

    abstract getTitle(): string;

    abstract getDescription(): string;

    abstract getVariants(): Variant[];

    getVariantNames(): string[] {
        let out: string[] = [];
        for (let variant of this.getVariants()) {
            out.push(variant.variantName);
        }
        return out;
    }

    getAlternatives(chord: Chord): Alternative[] {
        return [new Alternative(0, '(No alternatives)')];
    }

    setNextAlternative1(chord: Chord) {
        if (this.alternative1 < this.getAlternatives(chord).length -1 ) {
            this.alternative1++;
        } else {
            this.alternative1 = 0;
        }
        if (this.alternative1 === this.alternative2) {
            this.setNextAlternative2(chord);
        }
    }

    setNextAlternative2(chord: Chord) {
        if (this.alternative2 < this.getAlternatives(chord).length -1 ) {
            this.alternative2++;
        } else {
            this.alternative2 = 0;
        }
        if (this.alternative1 === this.alternative2) {
            this.setNextAlternative1(chord);
        }
    }

    resetAlternative1() {
        this.alternative1 = 0;
    }

    resetAlternative2() {
        this.alternative2 = 0;
    }

    getAlternative1() {
        return this.alternative1;
    }

    getAlternative2() {
        return this.alternative2;
    }

    /**
     * The lowest pitch that the Harmonizer will accept.
     */
    getLowerLimit(): number {
        return 43;
    }

    /**
     * The highest pitch that the Harmonizer will accept.
     */
    getUpperLimit(): number {
        return 83;
    }

    /**
     * The number of voices that is normally desired with this Harmonizer.
     */
    getDesiredVoiceCount(): number {
        return undefined;
    }

    setVariantNumber(variantNumber: number) {
        if (
            variantNumber >= this.getVariants().length ||
            variantNumber < 0 ||
            variantNumber !== Math.floor(variantNumber)
        ) {
            throw RangeError;
        }
        this.variantNumber = variantNumber;
    }

    getParams() {
        return this.getVariants()[this.variantNumber].paramList;
    }

    deleteVoices(chord: Chord) {
        chord.voicing.clear();
    }

    // The index is only needed if we occasionally want to insert a Voice somewhere else
    // than at the top. Test!
    addVoice(chord: Chord, voice: Voice) {
        // Todo: if anything goes wrong, return false!

        // First we need to find a pitch for the Voice. For this, we'll need a test pitch.
        let testPitch: number;

        // If this is the first Voice in the Voicing, set testPitch to lowerLimit.
        if (chord.voicing.isEmpty()) {
            testPitch = this.getLowerLimit();
        } else {
            // testPitch must be above the pitch of the topmost Voice.
            testPitch = chord.voicing.last().pitch + 1;
        }

        // Loop until we (hopefully) find a suitable pitch.
        while (testPitch <= this.getUpperLimit()) {
            if (pc(testPitch) === voice.element.pitchClass && isOnTopOf(testPitch, chord.voicing.last())) {
                voice.pitch = testPitch;
                return chord.voicing.add(voice);
            }
            testPitch++;
        }

        // If we finished the loop without finding a suitable pitch, we give up.
        return false;
    }

    insertVoice(chord: Chord, newVoice: Voice) {
        for (let index: number = 0; index < chord.voicing.size(); index++) {
            if (chord.voicing.elementAtIndex(index).pitch > newVoice.pitch) {
                continue;
            }
            if (chord.voicing.elementAtIndex(index).pitch === newVoice.pitch) {
                // Nothing to do!
                return true;
            }
            return chord.voicing.add(newVoice, index + 1 );
        }
        // This should only happen if the voicing is empty.
        return false;
    }

    checkSmallSecondInTop(chord: Chord) {
        /* Usually we don't want a small second in top of a voicing. */
        let topVoice: Voice = chord.voicing.last();
        let secondVoice: Voice = chord.voicing.elementAtIndex(chord.voicing.size() - 2);
        let topInterval: number = topVoice.pitch - secondVoice.pitch;
        if (topInterval === 1) {
            return true;
        }
        return false;
    }

    dropTopVoice(chord: Chord) {
        /* We move the top voice down. Of course, sometimes you might want to move
        * the 2.nd voice down instead.
        */
        let voiceToMove: Voice = chord.voicing.last();
        chord.voicing.remove(voiceToMove);
        /* Maybe it's a bad idea to change the pitch directly. It would be better to
        * make the pitch a private property of Voice, and move it trough octaveDown
        * and octaveUp methods on Voice.
        */
        voiceToMove.pitch -= 12;
        return this.insertVoice(chord, voiceToMove);
}

    addVoiceFromElementNumber(chord: Chord, elementNumber: number) {
        let voice: Voice;
        if (!chord.elements.containsKey(elementNumber)) {
            if (elementNumber === 3) {
                // If element #3 is missing, check for a sus4.
                if (chord.elements.containsKey(4)) {
                    voice = new Voice();
                    voice.element = chord.elements.getValue(4);
                    return this.addVoice(chord, voice);
                }
            }
            return false;
        }
        // If we get here, elementNumber is OK.
        voice = new Voice();
        voice.element = chord.elements.getValue(elementNumber);
        return this.addVoice(chord, voice);
    }

    /* Adds a stack of thirds to the chords' voicing, starting at elementNumber.
    * Only adds voices that are not already placed.
    */
    addStackOfThirds(chord: Chord, firstElementNumber: number) {
        for (let elementNumber: number = firstElementNumber; elementNumber <= 13; elementNumber += 2) {
            this.addVoiceFromElementNumber(chord, elementNumber);
        }
    }

    /* Finishes the voicing by adding voices referring to unreferred elements.
    * Always starts by looking for element #1.
    */
    addMissingVoices(chord: Chord) {
        for (let elementNumber: number = 1; elementNumber <= 13; elementNumber++) {
            if (chord.elements.containsKey(elementNumber)) {
                let voice: Voice = new Voice();
                voice.element = chord.elements.getValue(elementNumber);
                if (!chord.voicing.contains(voice, voicesAreEqualByPitchClass)) {
                    this.addVoice(chord, voice);
                }
            }
        }
    }

    /* True if the voice count on Chord >= the harmonizers' desired number of voices. */
    voicingIsFull(chord: Chord) {
        return chord.getVoicingSize() >= this.getDesiredVoiceCount();
    }

    /* True if the voice count on Chord > the harmonizers' desired number of voices. */
    voicingIsCrowded(chord: Chord) {
        return chord.getVoicingSize() > this.getDesiredVoiceCount();
    }

    /**
     * Takes a chord as argument and adds voices to it.
     * @param chord
     * @returns A boolean indicating if the harmonization went well or not.
     * A 'false' value will mean that the could could not be harmonized using
     * this harmonizer (not all Chords can be harmonized with all Harmonizers
     * in all variants.)
     */
    abstract harmonize(chord: Chord): boolean;
}