import { BaseElement } from './base_element';
import { ThirdType, FifthType, SeventhType, Chord } from './chord';

let chord: Chord = new Chord(new BaseElement(0));
chord.primarySymbol = 4;
chord.bender = 0;
chord.thirdType = ThirdType.minor;
chord.seventhType = SeventhType.dominant;
chord.fifthType = FifthType.b5;
console.log(chord.getChordName());
chord.build();
console.log(chord);