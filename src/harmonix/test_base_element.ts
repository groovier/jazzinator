import { BaseElement } from './base_element';

let e1 = new BaseElement(BaseElement.nameToVal('c'));
console.log(e1.getName());
let e2 = e1.step(4);
console.log(e2.getName());
let e3 = e2.step(3);
console.log(e3.getName());
let e4 = e3.step(3);
e4.sharpNature = false;
console.log(e4.getName());