/// <reference path="./interfaces.d.ts" />

import * as React from 'react';

import { Chord } from './harmonix/chord';

interface KeyProps {
    geometry: string;
    octave_number: number;
    pitchClass: number;
    chord: Chord;
}

interface OctaveProps {
    octave_number: number;
    chord: Chord;
}

interface KeyState {
    pitch: number;
}

class Key extends React.Component<KeyProps, KeyState> {

    constructor(props: KeyProps) {
        super(props);
        this.state = {pitch: this.props.octave_number * 12 + this.props.pitchClass};
    }

    shouldIPlay() {
        for (let voice of this.props.chord.getVoices()) {
            if (this.state.pitch === voice.pitch) {
                return true;
            }
        }
        return false;
    }

    getMyRoleNumber() {
        if (!this.shouldIPlay()) {
            return null;
        }
        return this.props.chord.getRoleNumber(this.props.pitchClass);
    }

    render() {
        return (
            <div
                className={
                    this.props.geometry
                    + (this.shouldIPlay() ? ' role_' + this.getMyRoleNumber() : '')
                }
            />
        );
    }
}

export class Octave extends React.Component<OctaveProps, {}> {

    constructor(props: OctaveProps) {
        super(props);
    }

    render() {
        return (
            <div className="octave_outer">
                <div className="octave">
                    <div className="white_container">
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={0}
                            geometry="white_key"
                        />
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={2}
                            geometry="white_key"
                        />
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={4}
                            geometry="white_key"
                        />
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={5}
                            geometry="white_key"
                        />
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={7}
                            geometry="white_key"
                        />
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={9}
                            geometry="white_key"
                        />
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={11}
                            geometry="white_key"
                        />
                    </div>
                    <div className="black_container two_black">
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={1}
                            geometry="black_key black_key_1"
                        />
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={3}
                            geometry="black_key black_key_1"
                        />
                    </div>
                    <div className="black_container three_black">
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={6}
                            geometry="black_key black_key_2"
                        />
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={8}
                            geometry="black_key black_key_2"
                        />
                        <Key
                            octave_number={this.props.octave_number}
                            chord={this.props.chord}
                            pitchClass={10}
                            geometry="black_key black_key_2"
                        />
                    </div>
                </div>
                <div className="octave_number">
                    {this.props.octave_number}
                    &nbsp;
                    {this.props.octave_number === 5 ? '(Center octave)' : null}
                </div>
            </div>
        );
    }
}

export default Octave;
