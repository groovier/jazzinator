/// <reference path="./interfaces.d.ts" />

import * as React from 'react';
import './App.css';
import { BaseElement } from './harmonix/base_element';
import { Chord } from './harmonix/chord';
import SettingsStore = require('./settings_store');

interface LEDContainerProps {
  baseElement: BaseElement;
  chord: Chord;
}

class LEDContainer extends React.Component<LEDContainerProps, {}> {

  getClassNames(pitchClass: number) {
    let classNames: string = 'led ';
    if (this.props.chord && this.props.chord.containsPitchClass(this.props.baseElement.pitchClass)) {
      classNames += 'on';
    } else {
      classNames += 'off';
    }
    return classNames;
  }

  render() {
      return (
        <div className="ledContainer">
            <div className={this.getClassNames(this.props.baseElement.pitchClass)} />
            <div className="ledLabel">{this.props.baseElement.getName(SettingsStore.getEuro())}</div>
        </div>
      );
  }
}

interface ChordPreviewerProps {
  chord: Chord;
}

export class ChordPreviewer extends React.Component<ChordPreviewerProps, {}> {

  render() {
    let sharpNature: boolean = false;
    if (this.props.chord && this.props.chord.elements.containsKey(1)) {
      sharpNature = this.props.chord.elements.getValue(1).sharpNature;
    }
    return (
        <div className="inator">
          <div className="inatorDescription">Chord Preview</div>
          <div className="ledArray">
          <LEDContainer
            baseElement={new BaseElement(0, sharpNature)}
            chord={this.props.chord}
          />
          <LEDContainer
            baseElement={new BaseElement(1, sharpNature)}
            chord={this.props.chord}
          />
          <LEDContainer
            baseElement={new BaseElement(2, sharpNature)}
            chord={this.props.chord}
          />
          <LEDContainer
            baseElement={new BaseElement(3, sharpNature)}
            chord={this.props.chord}
          />
          <LEDContainer
            baseElement={new BaseElement(4, sharpNature)}
            chord={this.props.chord}
          />
          <LEDContainer
            baseElement={new BaseElement(5, sharpNature)}
            chord={this.props.chord}
          />
          <LEDContainer
            baseElement={new BaseElement(6, sharpNature)}
            chord={this.props.chord}
          />
          <LEDContainer
            baseElement={new BaseElement(7, sharpNature)}
            chord={this.props.chord}
          />
          <LEDContainer
            baseElement={new BaseElement(8, sharpNature)}
            chord={this.props.chord}
          />
          <LEDContainer
            baseElement={new BaseElement(9, sharpNature)}
            chord={this.props.chord}
          />
          <LEDContainer
            baseElement={new BaseElement(10, sharpNature)}
            chord={this.props.chord}
          />
          <LEDContainer
            baseElement={new BaseElement(11, sharpNature)}
            chord={this.props.chord}
          />
          </div>
        </div>
    );
  }

}
