/// <reference path="./interfaces.d.ts" />

import * as React from 'react';
import './App.css';

import { ChordDefinator } from './chord_definator';
import { Chord } from './harmonix/chord';
import SettingsStore = require('./settings_store');
import { BaseElement } from './harmonix/base_element';
import { VoicingDefinator } from './voicing_definator';
import { Octave } from './octave';
import { BasicJazz } from './harmonix/basic_jazz';

interface AppState {
  chord: Chord;
}

class App extends React.Component<{}, AppState> {

  constructor(props: {}) {
    super(props);
    let chord: Chord = new Chord(new BaseElement(0));
    let harmonizer: BasicJazz = new BasicJazz();
    chord.voicer = harmonizer;
    this.state = {
      chord: chord
    };
  }

  onChordDefinatorChanged(chord: Chord) {
    this.setState({chord: chord});
    this.state.chord.build();
  }

  onVoicingDefinatorChanged(chord: Chord) {
    this.setState({chord: chord});
    // this.state.chord.voicer.harmonize(this.state.chord);
  }

  onEuropeanChanged(e: React.ChangeEvent<HTMLInputElement>) {
    SettingsStore.setEuro(!SettingsStore.getEuro());
    this.forceUpdate();
  }

  componentWillUpdate() {
    if (this.state.chord.voicer) {
      this.state.chord.voicer.harmonize(this.state.chord);
    }
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h1>The Jazzinator</h1>
          <h2>&nbsp;Jazz up your piano chords...</h2>
        </div>
        <div>
          <div className="inator">
            <label>Use European notation:
              <input
                type="checkbox"
                checked={SettingsStore.getEuro()}
                onChange={e => this.onEuropeanChanged(e)}
              />
            </label>
          </div>
          <ChordDefinator
            chord={this.state.chord}
            callbackParent={(chord: Chord) => this.onChordDefinatorChanged(chord)}
          />
          <VoicingDefinator
            chord={this.state.chord}
            callbackParent={(chord: Chord) => this.onVoicingDefinatorChanged(chord)}
          />
          <div className="piano">
            <Octave octave_number={3} chord={this.state.chord} />
            <Octave octave_number={4} chord={this.state.chord} />
            <Octave octave_number={5} chord={this.state.chord} />
            <Octave octave_number={6} chord={this.state.chord} />
          </div>
        </div>
      </div>
    );
  }

}

export default App;
