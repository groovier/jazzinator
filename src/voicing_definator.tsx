/// <reference path="./interfaces.d.ts" />

import * as React from 'react';

import { Chord } from './harmonix/chord';
import { StyleBook } from './harmonix/stylebook';
import { generateOptions } from './utils';

interface VoicingDefinatorProps {
  chord: Chord;
  callbackParent: Function;
}

interface VoicingDefinatorState {
  chord: Chord;
  stylebook: StyleBook;
  style_id: number;
  variant_number: number;
}

export class VoicingDefinator extends React.Component<VoicingDefinatorProps, VoicingDefinatorState> {

  constructor(props: VoicingDefinatorProps) {
    super(props);
    let stylebook: StyleBook = new StyleBook();
    let voicer = stylebook.styles[3];  // Set style 2 as the initial style
    this.props.chord.voicer = voicer;
    this.state = {
      chord: this.props.chord,
      stylebook: stylebook,
      style_id: 3,  // Todo: remove
      variant_number: 0,  // Todo: remove
    };
  }
  
  handleStyleChange(e: React.ChangeEvent<HTMLSelectElement>, instance: VoicingDefinator) {
    let styleId: number = parseInt(e.target.value, 10);
    this.state.chord.voicer = this.state.stylebook.styles[styleId];
    instance.setState({
      style_id: styleId,
      variant_number: 0
    });
    this.props.callbackParent(this.state.chord);
  }

  handleVariantChange(e: React.ChangeEvent<HTMLSelectElement>, instance: VoicingDefinator) {
    let variantNumber: number = parseInt(e.target.value, 10);
    // Todo: whay do I sometimes have to use instance instead of this?
    // At least it should be consistent!
    instance.state.chord.voicer.variantNumber = variantNumber;
    instance.setState({
      variant_number: variantNumber
    });
    instance.props.callbackParent(instance.state.chord);
  }

  handleExt1AlternativeNextClick(e: React.MouseEvent<HTMLButtonElement>, instance: VoicingDefinator) {
    this.state.chord.voicer.setNextAlternative1(instance.state.chord);
    instance.props.callbackParent(instance.state.chord);
  }

  handleExt2AlternativeNextClick(e: React.MouseEvent<HTMLButtonElement>, instance: VoicingDefinator) {
    this.state.chord.voicer.setNextAlternative2(instance.state.chord);
    instance.props.callbackParent(instance.state.chord);
  }

  render() {
    let stylebook = this.state.stylebook;
    let handleVariantChange = this.handleVariantChange;
    return (
      <div className="inator">
        <div className="columnContainer">
          <div className="sectionHeader">Voicing Display</div>
          <div className="display">
          <h2>
            {this.state.stylebook.styles[this.state.style_id].getTitle()}
          </h2>
          <p>
            {this.state.stylebook.styles[this.state.style_id].getDescription()}
          </p>
          </div>
        </div>
        <div className="columnContainer">
          <div className="sectionHeader">Voicing Selection</div>
          <div className="innerColumnContainer">
            <div className="rowContainer">
              <div className="control">
                <label>
                  Voicing Style:
                </label>
                <select value={this.state.style_id} onChange={e => this.handleStyleChange(e, this)}>
                  {generateOptions(stylebook.index())}
                </select>
              </div>
              <div className="control">
                <label>
                  Variant:
                </label>
                <select value={this.state.variant_number} onChange={e => handleVariantChange(e, this)}>
                  {generateOptions(stylebook.styles[this.state.style_id].getVariantNames())}
                </select>
              </div>
            </div>
            <div className="rowContainer">
              <div className="control">
                <label>
                  Ext1 alternatives:
                </label>
                <button
                  className="alternatives"
                  onClick={e => this.handleExt1AlternativeNextClick(e, this)}
                  disabled={this.state.chord.voicer.getAlternatives(this.state.chord).length === 1}
                >
                  {this.state.chord.voicer.getAlternatives(this.state.chord)[this.state.chord.voicer.getAlternative1()]
                    .description}
                </button>
              </div>
              <div className="control">
                <label>
                  Ext2 alternatives:
                </label>
                <button
                  className="alternatives"
                  onClick={e => this.handleExt2AlternativeNextClick(e, this)}
                  disabled={this.state.chord.voicer.getAlternatives(this.state.chord).length === 1}
                >
                  {this.state.chord.voicer.getAlternatives(this.state.chord)[this.state.chord.voicer.getAlternative2()]
                    .description}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}
