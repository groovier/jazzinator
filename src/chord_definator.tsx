/// <reference path="./interfaces.d.ts" />

import * as React from 'react';
import './App.css';

import { Chord } from './harmonix/chord';
import { generateOptions, generateOptionsWithValues } from './utils';
import SettingsStore = require('./settings_store');

interface ChordDefinatorProps {
  chord: Chord;
  callbackParent: Function;
}

interface ChordDefinatorState {
  chord: Chord;
  benderOptions: [number, string][];
}

export class ChordDefinator extends React.Component<ChordDefinatorProps, ChordDefinatorState> {
  constructor(props: {chord: Chord, callbackParent: Function}) {
    super(props);
    this.state = {
      chord: this.props.chord,
       // Since the default prinmary symbol is C, we do not give the 'flat' option
      benderOptions: [[1, ''], [2, 'sharp']]
    };
    // Immediately call the parent to make shure it has a ChordBuilder
    this.props.callbackParent(this.state.chord);
  }

  onPrimarySymbolChanged(e: React.ChangeEvent<HTMLSelectElement>) {
    this.state.chord.primarySymbol = parseInt(e.target.value, 10); // we update our state
    // Set bender options according to primary symbol
    switch (this.state.chord.primarySymbol) {
      case 0:  // c
        this.setState({benderOptions: [[1, ''], [2, 'sharp']]});
        break;
      case 2:  // e
        this.setState({benderOptions: [[0, 'flat'], [1, '']]});
        break;
      case 3:  // f
        this.setState({benderOptions: [[1, ''], [2, 'sharp']]});
        break;
      case 6:  // b
        this.setState({benderOptions: [[0, 'flat'], [1, '']]});
        break;
      default:
      this.setState({benderOptions: [[0, 'flat'], [1, ''], [2, 'sharp']]});
    }
    // Set the bender in neutral position since this is the only option always available
    this.state.chord.bender = 1;
    this.props.callbackParent(this.state.chord); // we notify our parent
  }

  onBenderChanged(e: React.ChangeEvent<HTMLSelectElement>) {
    this.state.chord.bender = parseInt(e.target.value, 10); // we update our state
    this.props.callbackParent(this.state.chord); // we notify our parent
  }

  onThirdTypeChanged(e: React.ChangeEvent<HTMLSelectElement>) {
    this.state.chord.thirdType = parseInt(e.target.value, 10); // we update our state
    this.props.callbackParent(this.state.chord); // we notify our parent
  }

  onFifthTypeChanged(e: React.ChangeEvent<HTMLSelectElement>) {
    this.state.chord.fifthType = parseInt(e.target.value, 10); // we update our state
    this.props.callbackParent(this.state.chord); // we notify our parent
  }

  onSeventhTypeChanged(e: React.ChangeEvent<HTMLSelectElement>) {
    this.state.chord.seventhType = parseInt(e.target.value, 10); // we update our state
    this.props.callbackParent(this.state.chord); // we notify our parent
  }

  onDimClick() {
    this.state.chord.thirdType = 0;
    this.state.chord.fifthType = 0;
    this.state.chord.seventhType = 0;
    this.props.callbackParent(this.state.chord);
  }

  onHalfDimClick() {
    this.state.chord.thirdType = 0;
    this.state.chord.fifthType = 0;
    this.state.chord.seventhType = 1;
    this.props.callbackParent(this.state.chord);
  }

  onTransposeHalfUpClick() {
    this.transposeHalfUp();
    this.props.callbackParent(this.state.chord);
  }

  onTransposeHalfDownClick() {
    this.transposeHalfDown();
    this.props.callbackParent(this.state.chord);
  }

  onTransposeWholeUpClick() {
    this.transposeHalfUp();
    this.transposeHalfUp();
    this.props.callbackParent(this.state.chord);
  }

  onTransposeWholeDownClick() {
    this.transposeHalfDown();
    this.transposeHalfDown();
    this.props.callbackParent(this.state.chord);
  }

  onTransposeFourUpClick() {
    this.transposeHalfUp();
    this.transposeHalfUp();
    this.transposeHalfUp();
    this.transposeHalfUp();
    this.transposeHalfUp();
    this.props.callbackParent(this.state.chord);
  }

  onTransposeFourDownClick() {
    this.transposeHalfDown();
    this.transposeHalfDown();
    this.transposeHalfDown();
    this.transposeHalfDown();
    this.transposeHalfDown();
    this.props.callbackParent(this.state.chord);
  }

  transposeHalfUp() {
    this.state.chord.bender++;
    // Handle bender overflow
    if (this.state.chord.bender === 3) {
      this.state.chord.bender = 1;
      this.state.chord.primarySymbol += 1;
    }
    // Handle E -> F transposition
    if (this.state.chord.primarySymbol === 2 && this.state.chord.bender === 2) {
      this.state.chord.primarySymbol = 3;
      this.state.chord.bender = 1;
    }
    // Handle B -> C transposition
    if (this.state.chord.primarySymbol === 6 && this.state.chord.bender === 2) {
      this.state.chord.primarySymbol = 0;
      this.state.chord.bender = 1;
    }
  }

  transposeHalfDown() {
    this.state.chord.bender--;
    // Handle bender underflow
    if (this.state.chord.bender === -1) {
      this.state.chord.bender = 1;
      this.state.chord.primarySymbol -= 1;
    }
    // Handle F -> E transposition
    if (this.state.chord.primarySymbol === 3 && this.state.chord.bender === 0) {
      this.state.chord.primarySymbol = 2;
      this.state.chord.bender = 1;
    }
    // Handle C -> B transposition
    if (this.state.chord.primarySymbol === 0 && this.state.chord.bender === 0) {
      this.state.chord.primarySymbol = 6;
      this.state.chord.bender = 1;
    }
  }

  render() {
    let chordLetterList: string[];
    chordLetterList = (Chord.getChordLetters(SettingsStore.getEuro()));
    let warningSixth = 'Rather than defining the chord as a major 6th chord, consider to define '
    + 'it as a major 7 or major maj7 chord and add the 13th as an extension.';
    return(
      <div className="inator">
        <div className="columnContainer">
          <div className="sectionHeader">Chord Display</div>
          <div className="display">
            <h2>
              {this.state.chord.getChordName(SettingsStore.getEuro())}
            </h2>
            <p>
            {this.state.chord.isMajor() && this.state.chord.isSixth() ? warningSixth : null}
            </p>
          </div>
        </div>
        <div className="columnContainer">
          <div className="sectionHeader">Chord Selection</div>
          <div className="innerColumnContainer">
            <div className="rowContainer">
              <select value={this.state.chord.primarySymbol} onChange={e => this.onPrimarySymbolChanged(e)}>
                {generateOptions(chordLetterList)}
              </select>
              <select value={this.state.chord.bender} onChange={e => this.onBenderChanged(e)}>
                {generateOptionsWithValues(this.state.benderOptions)}
              </select>
              <select value={this.state.chord.thirdType} onChange={e => this.onThirdTypeChanged(e)}>
                {generateOptions(['minor', 'major', 'sus4'])}
              </select>
              <select value={this.state.chord.seventhType} onChange={e => this.onSeventhTypeChanged(e)}>
                {generateOptions(['6', '7', '-', 'maj7'])}
              </select>
              <select value={this.state.chord.fifthType} onChange={e => this.onFifthTypeChanged(e)}>
                {generateOptions(['b5', '', '+'])}
              </select>
            </div>
            <div className="rowContainer">
              <button id="dim" onClick={e => this.onDimClick()}>Dim (o)</button>
              <button id="halfDim" onClick={e => this.onHalfDimClick()}>Half-Dim (ø)</button>
            </div>
          </div>
        </div>
        <div className="columnContainer">
          <div className="sectionHeader">Transpose Functions</div>
          <div className="innerColumnContainer">
            <div className="rowContainer">
              <button onClick={e => this.onTransposeHalfUpClick()}>1/2 Up</button>
              <button onClick={e => this.onTransposeWholeUpClick()}>1/1 Up</button>
              <button onClick={e => this.onTransposeFourUpClick()}>4 Up</button>
            </div>
            <div className="rowContainer">
              <button onClick={e => this.onTransposeHalfDownClick()}>1/2 Down</button>
              <button onClick={e => this.onTransposeWholeDownClick()}>1/1 Down</button>
              <button onClick={e => this.onTransposeFourDownClick()}>4 Down</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}