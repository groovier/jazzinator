/// <reference path="./interfaces.d.ts" />

import * as React from 'react';

/**
 * Utility for generating select options from an array.
 * @param optArray Array with options as strings.
 */
export function generateOptions(optArray: string[]) {
    return optArray.map((x: string) => (
        <option key={optArray.indexOf(x)} value={optArray.indexOf(x)}>{x}</option>
    ));
}

export function generateOptionsWithValues(optArray: [number, string][]) {
    return optArray.map((x: [number, string]) => (
        <option key={x[0]} value={x[0]}>{x[1]}</option>
    ));
}