import { StoreBase, AutoSubscribeStore, autoSubscribe } from 'resub';

@AutoSubscribeStore
class SettingsStore extends StoreBase {
    private euro: boolean;

    setEuro(euro: boolean) {
        this.euro = euro;
        this.trigger();
    }

    @autoSubscribe
    getEuro() {
        return this.euro;
    }
}

export = new SettingsStore();